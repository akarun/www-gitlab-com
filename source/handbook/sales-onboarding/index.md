---
layout: markdown_page
title: "Sales Onboarding"
---
1. Domain and Product Training
    * [GitLab Ecosystem](https://docs.google.com/presentation/d/1vCU-NbZWz8NTNK8Vu3y4zGMAHb5DpC8PE5mHtw1PWfI/edit)
    * [GitLab Basics](http://doc.gitlab.com/ce/gitlab-basics/README.html)
    * [GitLab Introduction to Terminology](https://about.gitlab.com/2015/05/18/simple-words-for-a-gitlab-newbie/)
    * [Intro to Git](https://www.codeschool.com/account/courses/try-git)
    * [Create a GitLab Account](https://courses.platzi.com/classes/git-gitlab/concepto/first-steps/create-an-account-on-gitlab/material/)
    * [Gitlab Workshop Part 1: Basics of Git and Gitlab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-1/material/)
    * [GitLab Workshop Part 2: Basics of Git and Gitlab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-23370/material/)
    * [Gitlab Workshop Part 3: Basics of Git and Gitlab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-3/material/)
    * [Gitlab Workshop Part 4: Basics of Git and Gitlab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-4/material/)
    * [Create and Add your SHH key to GitLab](https://www.youtube.com/watch?v=54mxyLo3Mqk)
    * [Creating a new project in GitLab](https://www.youtube.com/watch?v=7p0hrpNaJ14)
    * [Issue and Merge Request](https://www.youtube.com/watch?v=raXvuwet78M)
    * [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)
    * [Client Demo of GitLab with Job and Haydn](https://gitlabmeetings.webex.com/cmp3000/webcomponents/jsp/docshow/closewindow.jsp)
    * [What is GitLab OmniBus?](https://www.youtube.com/watch?v=XTmpKudd-Oo)
    * [How to install GitLab with Omnibus](https://www.youtube.com/watch?v=Q69YaOjqNhg)
    * [Managing permissions within EE](https://www.youtube.com/watch?v=DjUoIrkiNuM)
    * [GitLab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/)
    * [GitLab Flow vs. Forking in GitLab](https://www.youtube.com/watch?v=UGotqAUACZA)
    * [Managing LDAP, Active Directory introduction with GitLab](https://www.youtube.com/watch?v=HPMjM-14qa8)
    * [GitLab University](https://about.gitlab.com/university/)

1. [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq)

1. [Comparison page on our website](https://about.gitlab.com/comparison/)

1. [Our Sales Process](https://about.gitlab.com/handbook/sales-process/)

1. [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)

1. Have your manager grant access to our accounting / finance apps [Recurly](https://app.recurly.com/login). Ask your buddy if they can do a screenshare the next time they process an order.

1. Have your manager grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs. In this folder, familiarize yourself with:

1. Login to [Salesforce.com](http://www.salesforce.com/), you should receive an email asking you to change your password:
    * Familiarize yourself with your custom view (https://na34.salesforce.com/00O61000001uYbM) of open opportunities for the month
    * Familiarize yourself with your custom view (https://na34.salesforce.com/00O61000001uYbR) of all open opportunities assigned to you.
    * Familiarize yourself with your custom view (https://na34.salesforce.com/00Q?fcf=00B610000027qT9&rolodexIndex=-1&page=1) of all your open leads.


1. [Our Sales Communication Guide](https://docs.google.com/document/d/1IMDzTj3hZrnsA417z9Ye7WBa8yLkWxGzaLZNJ3O_nVA/edit#heading=h.3nffcmsbeqo7)

1. [Giving a GitLab demo](https://about.gitlab.com/handbook/demo/)

1. [Support and development process](/handbook/support-and-development-process)