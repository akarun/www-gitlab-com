---
layout: markdown_page
title: "New Hire Day One - US Employees Only"
---

1. HR will complete and submit an online Add New Hire Form (TriNet Passport=>My Workplace=> Add New Hire/Rehire). This will generate the welcome email to the employee at their work email on their first date of hire.

1. Employee completes New Hire TriNet Passport 7 Steps Guide. The I-9 portion of this must be completed within the first two days of hire. Note: this is critical so you must contact HR@gitlab.com if you have difficulty with this form.

1. Employee submits to HR@gitlab.com a completed New Employee Personal Information Form.

1. HR will send a welcome letter which includes a link to the TriNet Employee Handbook and a link to the GitLab Handbook.

1. Please read through the New Hire Benefits Guidebook. This will go over medical, dental, vision and voluntary benefits.

1. Operations will reach out to the new hire within 48 hours of hire to identify and order any necessary supplies/equipment.

1. Each employee will be assigned a GitLab support person who will act as his/her ambassador for the first month of work. This person will help the new hire with finding the information that often makes the first month overwhelming.

If you have any questions or need help within the TriNet system please contact the Employee Solution Center at 800-638-0461 or email them at employees@trinet.com. 


