---
layout: markdown_page
title: "Developer Responsibilities and Tasks"
---

At GitLab, developers are highly independent and self-organized individual 
contributors who work together as a tight team in a [remote and agile](https://about.gitlab.com/2015/09/14/remote-agile-at-gitlab/) way.


## General Developer Responsibilities

* Develop features from request to polished end result.
* Support our [service engineers](https://about.gitlab/com/jobs/service-engineer) in getting to the bottom of user-reported issues and come up with robust solutions.
* Engage with the rest of the core team and the open source community and collaborate on improving GitLab.
* Review code contributed by the rest of the community and work with them to get it ready for production.
* Write documentation around features and configuration to save our users time.
* Take initiative in improving the software in small or large ways to address pain points in your own experience as a developer.
* Keep code easy to maintain and keep it easy for others to contribute code to GitLab.
* Qualify developers for hiring.

## Responsibilities Split Between Various Developers
* Omnibus packaging
* Chef cookbooks
* DevOps: ensuring that deployments are smooth and scalable
* Maintenance of various servers such as 
   * license.gitlab.com
   * version.gitlab.com
   * status.gitlab.com
   * doc.gitlab.com
   * dev.gitlab.org
* Feature development for GitLab CE, EE, and CI.