---
layout: markdown_page
title: "Front-End Engineer"
---

## Responsibilities

* Implement the interfaces in GitLab proposed by UX Engineers and contributors
* Improve the [static website of GitLab](https://about.gitlab.com/) based on the suggestions of the Graphic Designer and CMO
* Continually improve the quality of GitLab

## Requirements

* Know how to use CSS effectively
* Know how to use javascript effectively
* Collaborate effectively with UX Designers, Developers and Graphic Designers
* Be able to work with the rest of the community
* Needs to have extensive knowledge of Rails
