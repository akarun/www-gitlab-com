---
layout: markdown_page
title: "Graphic Designer"
---

## Responsibilities

* Set a the house style of GitLab
* Improve the looks of GitLab the application
* Design a coherent brand image (logo's, icons, colors, typography, etc.)
* Improve the design and layout of [our static website](https://about.gitlab.com/)
* Design our marketing materials
* Design our swag (t-shirts, stickers, etc.)
* Design for special occasions (product releases, events, etc.)
* Create exciting marketing materials
* Create printed materials (business cards, banners, brochures)
* Create web advertisements

## Tools

Abobe Photoshop, Illustrator, InDesign, Pre-press knowledge, some illustration skills. Ability to work and be happy with print stuff such as:

Graphic Designer - who can do all that support work for marketing and all things unrelated to a product. So from graphic designer you want to have:

## Requirements

* Can create vector artwork
* Create anything from quick mockups to pixel-perfect work
* Collaborate with UX Designers, Front-End Engineers and the rest of the community
