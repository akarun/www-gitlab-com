---
layout: markdown_page
title: University
---

## What is GitLab University

Version control will be the standard for creation of any type of media in the
future. Today it's still pretty hard to master.

GitLab University has as a goal to teach Git, GitLab and everything that relates
to that to anyone willing to listen and participate.

## [Classes](/university/classes)

Right now, there is a GitLab University class every Thursday at 5PM UTC.
To sign up, send Job a message.

If you're interested in any of these, would like to teach in one of them or
participate or help in any way, please contact Job or submit a merge request.

- [View all classes](/university/classes)

## Upcoming Topics

Please submit a merge request for suggestions.

- HA vs. Scalability
- Groups, Projects, Repositories
- JIRA and Jenkins integrations in GitLab

## Resources

- [GitLab documentation](http://doc.gitlab.com/)
- [Innersourcing article](https://about.gitlab.com/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/)
- [Platzi training course](https://courses.platzi.com/courses/git-gitlab/)
- [GitLab flow](http://doc.gitlab.com/ee/workflow/gitlab_flow.html)
- [Sales Onboarding materials](https://about.gitlab.com/handbook/sales-onboarding/)
- [GitLab Direction](https://about.gitlab.com/direction/)
- [GitLab compared to other tools](https://about.gitlab.com/comparison/)
